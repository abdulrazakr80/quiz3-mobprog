package yudiwbs.edu.upi.cs.quiz3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


    public class MainActivity extends AppCompatActivity {

    Toolbar myToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_main);
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mIsiNama:
                Toast.makeText(getApplicationContext(), "Nama Terisi..", Toast.LENGTH_LONG).show();
                //lengkapi

                return true;
            case R.id.mIsiTabel:
                Toast.makeText(getApplicationContext(), "Nama dan alamat terisi", Toast.LENGTH_LONG).show();
                //lengkapi
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
